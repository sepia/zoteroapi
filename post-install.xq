xquery version "3.1";

(: The following external variables are set by the repo:deploy function :)
(: the target collection into which the app is deployed :)
declare variable $target external;

import module namespace xrest="http://exquery.org/ns/restxq/exist" at "java:org.exist.extensions.exquery.restxq.impl.xquery.exist.ExistRestXqModule";

(: copy config template :)
xmldb:copy-resource($target, "config.tmpl.xml", $target, "config.xml"),

(:  register rest module :)
xrest:register-module(xs:anyURI($target || "/modules/rest.xql"))