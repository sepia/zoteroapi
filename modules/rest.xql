xquery version "3.1";

(:~
 :  ____    _                 _   ___ ___    __                   _    _      _ _    
 : |_  /___| |_ ___ _ _ ___  /_\ | _ \_ _|  / _|___ _ _   _____ _(_)__| |_ __| | |__ 
 :  / // _ \  _/ -_) '_/ _ \/ _ \|  _/| |  |  _/ _ \ '_| / -_) \ / (_-<  _/ _` | '_ \
 : /___\___/\__\___|_| \___/_/ \_\_| |___| |_| \___/_|   \___/_\_\_/__/\__\__,_|_.__/
 :                                                                                  
 :  REST interface of this module
 :
 : @author Johannes Biermann
 : @version 1.0
 :)

module namespace zrest="http://sade.textgrid.de/ns/zoteroapi/zoterorest";
declare namespace rest="http://exquery.org/ns/restxq";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
import module namespace util="http://exist-db.org/xquery/util";
import module namespace http="http://expath.org/ns/http-client";

import module namespace zotero="http://sade.textgrid.de/ns/zoteroapi/zotero" at "zotero.xqm";
import module namespace config="http://sade.textgrid.de/ns/zoteroapi/config" at "config.xqm";

declare variable $zrest:jcollection := config:get("zotero.datadirRoot") || "/" || config:get("zotero.datacollection") || '/json/';

(: 
 : Gets a single zotero item as Json
 : itemId = Zotero id
 :)
declare 
    %rest:path("/zotero/item/{$itemId}") 
    %rest:GET 
    %output:media-type ('application/json')
    %output:method("binary")
function zrest:zotero($itemId) {
    let $file := $zrest:jcollection || $itemId ||".json"
    return 
    if (zrest:checkAndSave($itemId)) then
        util:binary-doc($file)
    else
        zrest:error404()
    
};

(: 
 : Gets multiple zotero item as Json objects
 : itemids must be seperated via comma
 : eg. XYZMBQ,KSKAWSA
 : items not found are skipped!
 :) 
declare 
    %rest:path("/zotero/items/{$itemIds}") 
    %rest:GET 
    %output:media-type ('application/json')
    %output:method("text")
function zrest:getmultiple($itemIds) {
    
    let $ids := tokenize(normalize-space($itemIds), ',')
                
    let $json := string-join (
        for $id in $ids
            return 
                if (zrest:checkAndSave($id)) then
                '"' || $id || '":' 
                || util:binary-to-string(util:binary-doc($zrest:jcollection || $id ||".json"))
                ||','
                else ()
        )

    let $log := util:log-system-out($json)

    let $jsonlength := string-length($json)
    return 
        if ($jsonlength = 0) then
            zrest:error404()
        else
            '{' || substring($json, 1, ($jsonlength -1)) || '}'
            
}; 

declare function zrest:checkAndSave($itemId) as xs:boolean {
    let $file := $zrest:jcollection || $itemId ||".json"
    let $avail := util:binary-doc-available($file)

    (: check if zotero ID is in cache, else try to download it :)
    let $dl := if ($avail eq false()) then
        zotero:fetchAndSaveItem($itemId)
        else ()
    return 
        util:binary-doc-available($file)
    
};

declare function zrest:error404() as element()* {
    let $resp :=
        <rest:response>
            <http:response status="404" reason="Zotero ID not found" message="Zotero ID not found">
                <http:header name="Content-Language" value="en"/>
                <http:header name="Content-Type" value="text/plain; charset=utf-8"/>
            </http:response>
        </rest:response>
    return $resp
    
};