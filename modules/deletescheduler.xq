xquery version "3.1";
(:~
 :  ____    _                 _   ___ ___    __                   _    _      _ _    
 : |_  /___| |_ ___ _ _ ___  /_\ | _ \_ _|  / _|___ _ _   _____ _(_)__| |_ __| | |__ 
 :  / // _ \  _/ -_) '_/ _ \/ _ \|  _/| |  |  _/ _ \ '_| / -_) \ / (_-<  _/ _` | '_ \
 : /___\___/\__\___|_| \___/_/ \_\_| |___| |_| \___/_|   \___/_\_\_/__/\__\__,_|_.__/
 :                                                                                  
 :  deletes scheduler for fetching new zotero items, but only in memory.
 :
 : @author Johannes Biermann
 : @version 1.0
 :)

let $result := scheduler:delete-scheduled-job("ZoteroCronJob")

return
<results>
   <result>{$result}</result>
   <scheduledjobs>{scheduler:get-scheduled-jobs()}</scheduledjobs>
</results> 