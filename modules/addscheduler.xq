xquery version "3.1";

(:~
 :  ____    _                 _   ___ ___    __                   _    _      _ _    
 : |_  /___| |_ ___ _ _ ___  /_\ | _ \_ _|  / _|___ _ _   _____ _(_)__| |_ __| | |__ 
 :  / // _ \  _/ -_) '_/ _ \/ _ \|  _/| |  |  _/ _ \ '_| / -_) \ / (_-<  _/ _` | '_ \
 : /___\___/\__\___|_| \___/_/ \_\_| |___| |_| \___/_|   \___/_\_\_/__/\__\__,_|_.__/
 :                                                                                  
 : adds a scheduler to periodically fetch new zotero items, but only in memory. 
 : better add it via conf.xml to the database.
 :
 : @author Johannes Biermann
 : @version 1.0
 :)

import module namespace config="http://sade.textgrid.de/ns/zoteroapi/config" at "config.xqm";


let $xquery-path := $config:app-root || '/modules/zotereoscheduler.xq'

(: crontable for fetching zotero items. Once a day is normally enough
 See https://exist-db.org/exist/apps/doc/scheduler#cron for documentation of the cron
 syntax
   :)
let $cron := '0 0 0 * * ?'

let $add := scheduler:schedule-xquery-cron-job($xquery-path, $cron, 'ZoteroCronJob')

return
<results>
   <xquery-path>{$xquery-path}</xquery-path>
   <cron>{$cron}</cron>
   <result>{$add}</result>
   <scheduledjobs>{scheduler:get-scheduled-jobs()}</scheduledjobs>
</results>